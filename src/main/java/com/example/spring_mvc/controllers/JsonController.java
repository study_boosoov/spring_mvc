package com.example.spring_mvc.controllers;

import com.example.spring_mvc.models.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/json")
public class JsonController {


    @GetMapping("/person")
    public Person greeting(@RequestParam(value = "name", defaultValue = "Sergey") String name) {
        return new Person(1L, name, 25);
    }
}
